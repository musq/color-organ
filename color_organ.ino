/*

    Example of use of the FFT libray to compute FFT for a signal sampled through the ADC.
        Copyright (C) 2017 Enrique Condes

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "arduinoFFT.h"

arduinoFFT FFT = arduinoFFT(); /* Create FFT object */
/*
These values can be changed in order to evaluate the functions
*/
#define MICROPHONE A0
const uint16_t samples = 128; //This value MUST ALWAYS be a power of 2
volatile double samplingFrequency = 5000;

int RED = 8;
int GREEN = 10;
int BLUE = 12;

/*
These are the input and output vectors
Input vectors receive computed results from FFT
*/
double vReal[samples];
double vImag[samples];


const uint8_t antiLogMatrixLengthMinueOne = 31;
const uint8_t antiLogMatrix[] = {
    0,    1,    2,    3,    4,    5,    7,    9,    12,
    15,    18,    22,    27,    32,    38,    44,    51,    58,
    67,    76,    86,    96,    108,    120,    134,    148,    163,
    180,    197,    216,    235,    255
};

double scale;

unsigned long time1;
unsigned long time2;
unsigned long time3;

int g1=0, g2=0, g3=0, b1=0, b2=0, b3=0;

void setup()
{
  Serial.begin(115200);
  Serial.println("Ready");

  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);

  scale = (antiLogMatrixLengthMinueOne * 1.0) / 256;
}

void loop()
{
  time1 = millis();
  for(uint16_t i =0;i<samples;i++)
  {
    vReal[i] = double(15*(analogRead(MICROPHONE)-450));
    vImag[i] = 0.0; //Imaginary part must be zeroed in case of looping to avoid wrong calculations and overflows
  }
  time2=millis();
  
  samplingFrequency = 1000 * ((float)(samples-1) / (time2-time1));
  
  FFT.Windowing(vReal, samples, FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */

  FFT.Compute(vReal, vImag, samples, FFT_FORWARD); /* Compute FFT */

  FFT.ComplexToMagnitude(vReal, vImag, samples); /* Compute magnitudes */

  double bass = (10/1) * FFT.BPFoutAvgMagnitude(vReal, 260.0, 2, samples, samplingFrequency, FFT_WIN_TYP_RECTANGLE);
  double midRange = (30/1) * FFT.BPFoutAvgMagnitude(vReal, 1000.0, 2, samples, samplingFrequency, FFT_WIN_TYP_RECTANGLE);
  double treble = (50/1) * FFT.BPFoutAvgMagnitude(vReal, 2500.0, 2, samples, samplingFrequency, FFT_WIN_TYP_RECTANGLE);

  uint8_t scaledBass = floor(bass * scale * 1.7);
  uint8_t scaledMidRange = floor(midRange * scale * 1.0);
  uint8_t scaledTreble = floor(treble * scale * 5);

  if (scaledBass > antiLogMatrixLengthMinueOne) {
    scaledBass = antiLogMatrixLengthMinueOne;
  }
  if (scaledMidRange > antiLogMatrixLengthMinueOne) {
    scaledMidRange = antiLogMatrixLengthMinueOne;
  }
  if (scaledTreble > antiLogMatrixLengthMinueOne) {
    scaledTreble = antiLogMatrixLengthMinueOne;
  }

  // b1 = (saturateOutput(antiLogMatrix[scaledBass] + 50) + b1) / 2;
  // g1 = (saturateOutput(antiLogMatrix[scaledTreble] + 50) + g1) / 2;


  b1 = (saturateOutput(antiLogMatrix[scaledBass] + 50, 1) + b2 + b3) / 3;
  g1 = (saturateOutput(antiLogMatrix[scaledTreble] + 50, 2) + g2 + g3) / 3;

  b3 = b2;
  b2 = b1;

  g3 = g2;
  g2 = g1;


  analogWrite(RED, b1);
  analogWrite(GREEN, saturateOutput(antiLogMatrix[scaledMidRange] + 10, 0));
  analogWrite(BLUE, g1);

  // analogWrite(RED, saturateOutput(antiLogMatrix[scaledBass] + 50));
  // analogWrite(GREEN, saturateOutput(antiLogMatrix[scaledMidRange] + 10));
  // analogWrite(BLUE, saturateOutput(antiLogMatrix[scaledTreble] + 50));

  time3 = millis();
  // Serial.println(time2-time1);
  // Serial.println(time3-time2);
  // Serial.println(time3-time1);
  // Serial.println(1000 * ((float)(samples-1) / (time2-time1)) , 4);
  // Serial.println(1000.0 / (time3-time1) , 4);

  Serial.println(saturateOutput(antiLogMatrix[scaledBass] + 50, 1));
  Serial.println(saturateOutput(antiLogMatrix[scaledMidRange] + 10, 0));
  Serial.println(saturateOutput(antiLogMatrix[scaledTreble] + 50, 2));
  Serial.println("*********");
  // Serial.println(bass, 6);
  // Serial.println(midRange, 6);
  // Serial.println(treble, 6);
  // Serial.println("*********");
}

uint8_t saturateOutput(uint8_t input, uint8_t type)
{
  if (input > 255) {
    if (type > 0) {
      return 225;
    }
    return 255;
  } else {
    if (input < 13) {
      return 0;
    } else {
      if (type > 0) {
        return input - 30;
      }
      return input;
    }
  }
}
